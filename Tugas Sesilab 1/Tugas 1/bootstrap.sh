#!/usr/bin/env bash
apt-get update
apt-get install -y python-software-properties
add-apt-repository -y ppa:ondrej/php
add-apt-repository -y ppa:ondrej/apache2
apt-get update
apt-get install -y php7.0 libapache2-mod-php7.0 php7.0-mcrypt php7.0-curl
mkdir /var/www/.opcache
awk '{gsub(/;opcache.enable=0/,"opcache.enable=1")}1' /etc/php/7.0/apache2/php.ini > temp.txt && mv temp.txt /etc/php/7.0/apache2/php.ini
awk '{gsub(/;opcache.enable_cli=0/,"opcache.enable_cli=1")}1' /etc/php/7.0/apache2/php.ini > temp.txt && mv temp.txt /etc/php/7.0/apache2/php.ini
awk '{gsub(/;opcache.file_cache=/,"opcache.file_cache=/var/www/.opcache")}1' /etc/php/7.0/apache2/php.ini > temp.txt && mv temp.txt /etc/php/7.0/apache2/php.ini
awk '{gsub(/;opcache.file_cache_only=0/,"opcache.file_cache_only=1")}1' /etc/php/7.0/apache2/php.ini > temp.txt && mv temp.txt /etc/php/7.0/apache2/php.ini
echo "<?php phpinfo(); ?>" >> /var/www/html/index.php
rm /var/www/html/index.html
service apache2 restart
