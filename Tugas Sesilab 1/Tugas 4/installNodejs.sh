#!/usr/bin/env bash
cd /tmp
curl -sSL https://deb.nodesource.com/setup_6.x -o nodejs.sh
cat /tmp/nodejs.sh | sudo -E bash -
sudo apt-get update
sudo apt-get install -y nodejs
