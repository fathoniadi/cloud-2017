#!/usr/bin/env bash
apt-get update
apt-get install -y python-software-properties
apt-get install -y php7.0 libapache2-mod-php7.0 apache2
apt-get install -y unzip

echo "Listen 8000" >> /etc/apache2/ports.conf
awk '{gsub(/80/,"8000")}1' /etc/apache2/sites-available/000-default.conf > temp.txt && mv temp.txt /etc/apache2/sites-available/ci.conf
awk '{gsub(/html/,"html/ci")}1' /etc/apache2/sites-available/ci.conf > temp.txt && mv temp.txt /etc/apache2/sites-available/ci.conf
a2ensite ci.conf
systemctl reload apache2